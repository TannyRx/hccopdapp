		WITH AllResultsCTE AS(
		/*Flowsheet Results*/
		SELECT PatientID
		,InpatientDataID
		,PatientEncounterID
		,RecordedDTS
		,'Flowsheet' AS ResultSourceDSC
		,[FEV1] AS FEV1NBR
		,[FVC] AS FVCNBR
		,[FEV1/FVC] AS FEV1OverFVCNBR
		,[FEV1 % Predicted] AS FEV1PecentPredictedNBR
		,[FVC % Predicted] AS FVCPecentPredictedNBR
		,[Is this reading post-bronchodilator?] AS PostBronchodilatorDSC

		FROM(
			SELECT pat.PatientID,frl.InpatientDataID,pe.PatientEncounterID,fm.RecordedDTS, fg.DisplayNM, MeasureTXT
			FROM SAM.Respiratory.COPDPopulationPatients pat
			INNER JOIN Epic.Clinical.FlowsheetRecordLink frl
			ON pat.PatientID=frl.PatientID
			LEFT JOIN Epic.Clinical.FlowsheetMeasure fm
			ON fm.FlowsheetDataID=frl.FlowsheetDataID
			LEFT JOIN Epic.Clinical.FlowsheetGroup fg
			ON fm.FlowsheetMeasureID=fg.FlowsheetMeasureID
			LEFT JOIN Epic.Encounter.PatientEncounter pe
			ON frl.InpatientDataID=pe.InpatientDataID

			WHERE fm.FlowsheetMeasureID IN('1064' --R FVC
							,'1065' -- R FEV1 
							,'1066' -- R FEV1/FVC CALCULATED
							,'210155000087'--R UPH AMB PUL SPIROMETRY % PREDICTED FEV1
							,'7563'--AMB R ASTHMA BRONCHODILATOR
							,'210155000047' --R UPH AMB PUL SPIROMETRY % PREDICTED FVC
							)
		) AS SourceTable

		PIVOT( 
		MIN(MeasureTXT)
		FOR DisplayNM IN ([FEV1],[FVC],[FEV1/FVC],[FEV1 % Predicted],[FVC % Predicted],[Is this reading post-bronchodilator?])
		) AS PivotTable

		UNION

		/*Procedure Results*/

		SELECT PatientID
		,InpatientDataID
		,PatientEncounterID
		,RecordedDTS
		,ResultSourceDSC
		,[5] AS FEV1NBR
		,[6] AS FVCNBR
		,[7] AS FEV1OverFVCNBR
		,[6495]  AS FEV1PecentPredictedNBR
		,[6496] AS FVCPecentPredictedNBR
		,PostBronchodilatorDSC

		FROM(
			SELECT DISTINCT p.PatientID
			,pe.InpatientDataID
			,pe.PatientEncounterID
			,COALESCE(p.ResultDTS, p.OrderInstantDTS) AS RecordedDTS 
			,CONCAT(p.ProcedureCD,' - ',p.ProcedureDSC) AS ResultSourceDSC
			,ResultTXT
			,c.ComponentID
			,CASE WHEN p.ProcedureCD IN('PFT18','PFT34','PFT13','PFT101') THEN 'Post-Bronchodilator' END AS PostBronchodilatorDSC

			FROM SAM.Respiratory.COPDPopulationPatients pat
			INNER JOIN Epic.Orders.Procedure1 p
			ON pat.PatientID=p.PatientID
			LEFT JOIN  Epic.Orders.Result r
			ON r.OrderProcedureID=p.OrderProcedureID
			LEFT JOIN Epic.Reference.Component c
			ON c.ComponentID=r.ComponentID
			LEFT JOIN Epic.Orders.ParentOrder po
			on p.OrderProcedureID=po.OrderID
			LEFT JOIN Epic.Encounter.PatientEncounter pe
			ON p.PatientEncounterID=pe.PatientEncounterID

			WHERE  c.ComponentID IN ('5'	--FEV1
			,'6'	--FVC
			,'7'	--FEV1/FVC
			,'6495' --FEV1 % PREDICTED
			,'6496'	--FVC % PREDICTED
			)
			AND ISNUMERIC(r.ResultTXT) = 1 --Removes improperly coded results
		) AS SourceTable

		PIVOT( 
		MIN(ResultTXT)
		FOR ComponentID IN ([5],[6],[7],[6495],[6496])
		) AS PivotTable
)

SELECT PatientID
		,InpatientDataID
		,PatientEncounterID
		,RecordedDTS
		, ResultSourceDSC
		,CAST(FEV1NBR AS FLOAT) AS FEV1NBR
		,CAST(FVCNBR AS FLOAT) AS FVCNBR
		,CAST(FEV1OverFVCNBR AS FLOAT) AS FEV1OverFVCNBR
		,CAST(FEV1PecentPredictedNBR AS FLOAT) AS FEV1PecentPredictedNBR 
		,CAST(FVCPecentPredictedNBR AS FLOAT) AS FVCPecentPredictedNBR
		,PostBronchodilatorDSC
,CASE WHEN CAST(FEV1OverFVCNBR AS FLOAT) >= 70 THEN 'N' ELSE 'Y' END AS ConfirmedSpirometryFLG --updated with new rule that ratio must be >= 70 for nonCOPD population
,CASE WHEN CAST(FEV1OverFVCNBR AS FLOAT) >= 70 THEN 0 ELSE 1 END AS ConfirmedSpirometryFLGVAL --updated with new rule that ratio must be >= 70 for nonCOPD population
FROM AllResultsCTE
WHERE ISNUMERIC(FEV1OverFVCNBR)=1